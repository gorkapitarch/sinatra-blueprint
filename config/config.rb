configure do
  set :site_title, 'Título del sitio'
  set :site_host, 'www.dominio.com'

  set :username, ENV['site_username'] || 'admin'
  set :password, ENV['site_password'] || 'admin123'
  set :token, ENV['site_token'] || Digest::MD5.hexdigest('$3Qr1tYt0k3n')
end

configure :development do
  set :bind, '0.0.0.0'
  set :port, 3000
end

configure do
  enable :sessions
  set :session_secret, 'wAi$j3/w:k91'
  use Rack::Flash, :sweep => true
end