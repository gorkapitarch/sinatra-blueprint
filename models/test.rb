class Test
  include DataMapper::Resource

  property :id,         Serial
  property :title,      String
  property :slug,       String
  property :body,       Text
  property :html,       Text
  property :created_at, DateTime
  property :updated_at, DateTime
end