DataMapper.setup(:default, ENV['HEROKU_POSTGRESQL_WHITE_URL'] || "sqlite3://#{Dir.pwd}/development.db")

models = %w(
  test
  )

models.each do |m|
  require_relative "#{m}"
end

DataMapper.finalize
DataMapper.auto_upgrade!