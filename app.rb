require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

require 'digest/md5'
require 'rack-flash'

# Recarga el sitio en cada petición.
require "sinatra/reloader" if development?

require_relative 'config/config'
require_relative 'helpers'
require_relative 'models/models'

# Procesa SASS.
get '/:name.css' do |name|
  sass "sass/#{name}".to_sym, :style => :expanded
end

# Cargamos todas las rutas.
require_relative 'routes/routes'